import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';

import logo from '../../assets/header/logo.png';
import searchbar from '../../assets/header/quill_search-alt.png';
import cart from '../../assets/header/f7_cart.png';
import user_icon from '../../assets/header/user_icon.png';

import HeaderCart from './HeaderCart.jsx';

const drawerWidth = 240;
const navItems = ['Home', 'Product', 'About', 'Pricing'];
const productCategories = ['Category 1', 'Category 2', 'Category 3'];

const styles = {
  button: {
    color: '#fff',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '12px',
    textTransform: 'capitalize',
    maxHeight: '34px',
    '&.MuiSelect-root': {
      border: 'none', // Remove the border for the Select component
    },
    '&:hover': {
      backgroundColor: '#3366cc',
    },
    '& .MuiButton-label': {
      display: 'flex',
      alignItems: 'center',
    },
  },
};

function Header(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [selectedProductCategory, setSelectedProductCategory] = React.useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const handleProductCategoryChange = (event) => {
    setSelectedProductCategory(event.target.value);
    setAnchorEl(null);
  };

  const handleProductMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleProductMenuClose = () => {
    setAnchorEl(null);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        MUI
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar component="nav" sx={{ backgroundColor: '#40BFFF' }}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: 'none', sm: 'block' },
              display: 'flex',
              alignItems: 'center',
              fontFamily: 'Aref Ruqaa Ink',
            }}
          >
            <img src={logo} alt="SS Mall Logo" style={{ marginRight: '8px', width: '25px', height: '25px' }} />
            SS Mall
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block', marginRight: '20px'} }}>
            <Button sx={styles.button}>
              Home
            </Button>

            {/* Ganti Select menjadi Button dengan menu dropdown */}
            <Button
              sx={styles.button}
              id="product-menu"
              aria-haspopup="true"
              aria-expanded={Boolean(anchorEl) ? 'true' : 'false'}
              onClick={handleProductMenuClick}
            >
              Product
              <ArrowDropDownIcon />
            </Button>
            <Menu
              id="product-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleProductMenuClose}
            >
              {productCategories.map((category) => (
                <MenuItem key={category} onClick={() => handleProductCategoryChange({ target: { value: category } })}>
                  {category}
                </MenuItem>
              ))}
            </Menu>

            <Button sx={styles.button}>
              About
            </Button>
            <Button sx={styles.button}>
              Pricing
            </Button>
          </Box>

          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            <IconButton color="inherit">
              <img src={searchbar} alt="Button 1" style={{ width: '25px', height: '25px' }} />
            </IconButton>
            <IconButton color="inherit">
              
              <HeaderCart/>
            </IconButton>
            <IconButton color="inherit">
              <img src={user_icon} alt="Button 3" style={{ width: '25px', height: '25px' }} />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </Box>
  );
}

Header.propTypes = {
  window: PropTypes.func,
};

export default Header;
