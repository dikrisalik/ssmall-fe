import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';

export default function PaymentConfirmationModal({ open, handleClose }) {
  const [loading, setLoading] = useState(false);
  const [paymentSuccess, setPaymentSuccess] = useState(false);

  const product = {
    name: 'Product Name',
    image: 'path/to/image.jpg',
    discountPrice: 25.99,
    originalPrice: 30.99,
  };

  const handleConfirm = () => {
    setLoading(true);

    // Simulasi proses pembayaran (gantilah dengan logika sesuai kebutuhan)
    setTimeout(() => {
      // Implementasikan logika untuk menangani konfirmasi pembayaran di sini
      console.log('Payment confirmed!');
      setLoading(false);
      setPaymentSuccess(true);

      // Menutup modal setelah beberapa detik
      setTimeout(() => {
        handleClose();
        setPaymentSuccess(false);
      }, 2000);
    }, 3000);
  };

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="sm" fullWidth sx={{ margin: 'auto' }}>
      <DialogTitle>
        <Typography variant="h6">Confirm Payment</Typography>
      </DialogTitle>
      <DialogContent>
        {loading ? (
          <Typography variant="body1">Processing payment...</Typography>
        ) : (
          <>
            <img src={product.image} alt={product.name} style={{ width: '100%', height: 'auto' }} />
            <Typography variant="h6" sx={{ marginTop: '8px', marginBottom: '16px' }}>
              {product.name}
            </Typography>
            <Typography variant="h5" sx={{ marginBottom: '8px', fontWeight: 'bold' }}>
              ${product.discountPrice}
            </Typography>
            <Typography variant="body2" sx={{ color: '#BFBFBF', textDecoration: 'line-through' }}>
              ${product.originalPrice}
            </Typography>
          </>
        )}
      </DialogContent>
      <DialogActions>
        {loading ? (
          <Button disabled>Loading...</Button>
        ) : (
          <>
            <Button onClick={handleClose} color="secondary">
              Cancel
            </Button>
            <Button
              onClick={handleConfirm}
              variant="contained"
              style={{ backgroundColor: '#40BFFF', color: 'white' }}
            >
              Confirm
            </Button>
          </>
        )}
      </DialogActions>
      {paymentSuccess && (
        <DialogContent>
          <Typography variant="body1" sx={{ textAlign: 'center', fontWeight: 'bold' }}>
            Payment Success!
          </Typography>
        </DialogContent>
      )}
    </Dialog>
  );
}
