// File HeaderCart.jsx
import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import DeleteIcon from '@mui/icons-material/Delete';
import Typography from '@mui/material/Typography';

// Import PaymentConfirmationModal
import PaymentConfirmationModal from './ConfirmModal.jsx';

export default function HeaderCart() {
  const [state, setState] = React.useState({
    right: false,
    isModalOpen: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const toggleModal = () => {
    setState({ ...state, isModalOpen: !state.isModalOpen });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 350 }}
      role="presentation"
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Box sx={{ p: 2 }}>
        <Typography variant="h2" sx={{ fontSize: '30px', margin: 0 }}>My Cart</Typography>
      </Box>
      <Box sx={{ backgroundColor: '#C2EAFF' }}>
        <Typography variant="body2" sx={{ fontSize: '13px', marginLeft: '18px' }}>Get your free shipping voucher</Typography>
      </Box>

      <Divider />

      {/* Produk */}
      <List>
        <ListItem disablePadding>
          <ListItemButton>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
              {/* Bagian Kiri (Image) */}
              <Box sx={{ flex: '0 0 auto', marginRight: '16px' }}>
                <ListItemIcon>
                  <ShoppingCartIcon style={{ fontSize: 40 }} />
                </ListItemIcon>
              </Box>

              {/* Bagian Kanan (Konten Nama, Harga, Tombol Checkout) */}
              <Box sx={{ flex: '1 1 auto' }}>
                <Box sx={{ display: 'flex', alignItems: 'center', marginBottom: '8px' }}>
                  <Typography variant="h3" sx={{ fontSize: '20px', margin: 0 }}>Product Name</Typography>
                  <ListItemIcon sx={{ marginLeft: 'auto' }}>
                    <DeleteIcon color="error" style={{ fontSize: 24 }} />
                  </ListItemIcon>
                </Box>

                <Box sx={{ display: 'flex', alignItems: 'center', marginTop: '8px' }}>
                  <Typography variant="h4" sx={{ fontSize: '25px' }}>$25.99</Typography>
                  <Typography variant="body2" sx={{ fontSize: '10px', color: '#BFBFBF', marginLeft: '8px', textDecoration: 'line-through' }}>$30.99</Typography>
                </Box>
                <Button fullWidth variant="contained" sx={{ backgroundColor: '#EC6D62', color: 'white', fontSize: '10px' }} onClick={toggleModal}>
                  Checkout
                </Button>
              </Box>
            </Box>
          </ListItemButton>
        </ListItem>
      </List>

      <Divider />

      {/* Gunakan PaymentConfirmationModal di dalam komponen */}
      <PaymentConfirmationModal open={state.isModalOpen} handleClose={toggleModal} />
    </Box>
  );

  return (
    <div>
      <Button onClick={toggleDrawer('right', true)}>
        <ShoppingCartIcon style={{ fontSize: 25 }} />
      </Button>
      <Drawer
        anchor="right"
        open={state['right']}
        onClose={toggleDrawer('right', false)}
      >
        {list('right')}
      </Drawer>
    </div>
  );
}
