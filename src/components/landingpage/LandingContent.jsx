import React from 'react';
import { Container, Grid, Card, CardMedia, CardContent, Typography, Button, Box } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';

// Contoh data produk (gantilah dengan data sesuai kebutuhan)
const products = [
  {
    id: 1,
    name: 'Product 1',
    image: 'path/to/image1.jpg',
    rating: 4.5,
    price: 19.99,
    discountPrice: 15.99,
  },
  {
    id: 2,
    name: 'Product 2',
    image: 'path/to/image2.jpg',
    rating: 3.8,
    price: 24.99,
    discountPrice: 20.00,
  },
  {
    id: 3,
    name: 'Product 3',
    image: 'path/to/image2.jpg',
    rating: 3.8,
    price: 24.99,
    discountPrice: 19.99,
  },
  {
    id: 4,
    name: 'Product 4',
    image: 'path/to/image4.jpg',
    rating: 4.2,
    price: 29.99,
    discountPrice: 25.99,
  },
  {
    id: 5,
    name: 'Product 5',
    image: 'path/to/image5.jpg',
    rating: 4.0,
    price: 22.99,
    discountPrice: 18.99,
  },
  {
    id: 6,
    name: 'Product 6',
    image: 'path/to/image6.jpg',
    rating: 4.6,
    price: 34.99,
    discountPrice: 29.99,
  },
  {
    id: 7,
    name: 'Product 7',
    image: 'path/to/image7.jpg',
    rating: 4.8,
    price: 39.99,
    discountPrice: 35.99,
  },
  {
    id: 8,
    name: 'Product 8',
    image: 'path/to/image8.jpg',
    rating: 3.5,
    price: 18.99,
    discountPrice: 15.00,
  },
  {
    id: 9,
    name: 'Product 9',
    image: 'path/to/image9.jpg',
    rating: 4.1,
    price: 26.99,
    discountPrice: 22.99,
  },
  // Tambahkan produk lain sesuai kebutuhan
];

const LandingContent = () => {
  const handleOrderClick = (productId) => {
    // Implementasikan logika untuk menangani klik tombol order
    console.log(`Order button clicked for product with ID ${productId}`);
  };

  const latestProducts = products.slice(0, 3); // Ambil 3 produk terbaru

  return (
    <Box component="main" sx={{ p: 3, marginTop: '30px' }}>
      <Container>
        <Grid container spacing={4}>
          {/* Tampilkan 3 produk terbaru dengan tata letak berbeda */}
          {latestProducts.map((product) => (
            <Grid item key={product.id} xs={12} sm={8} md={6} lg={4}>
              <Card>
                <Grid container>
                  <Grid item xs={12} sm={6}>
                    <CardMedia
                      component="img"
                      height="140"
                      image={product.image}
                      alt={product.name}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                      <Typography variant="body1" sx={{ fontSize: '12px', fontWeight: 'bold' }}>{product.name}</Typography>
                      <Typography variant="body2" sx={{ display: 'flex', alignItems: 'center', fontSize: '12px' }}>
                        <StarIcon sx={{ color: '#FFD700', marginRight: '4px' }} />
                        {product.rating.toFixed(1)}
                      </Typography>
                      <Typography variant="body2" sx={{ display: 'flex', fontSize: '12px', marginTop: '8px' }}>
                        <span style={{ fontSize: '16px', color: 'black', marginRight: '4px',  fontWeight: 'bold'}}>
                          ${product.discountPrice.toFixed(2)}
                        </span>

                        <span style={{ fontSize: '10px', textDecoration: 'line-through', color: 'grey' }}>
                          ${product.price.toFixed(2)}
                        </span>
                      </Typography>
                      <Button onClick={() => handleOrderClick(product.id)} fullWidth variant="contained" sx={{ backgroundColor: '#40BFFF', maxHeight: '25px', fontSize: '12px', marginTop: '8px' }}>
                        Add to cart
                      </Button>
                    </CardContent>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          ))}

          {/* Tampilkan produk lainnya dengan tata letak seperti sebelumnya */}
          {products.slice(3).map((product) => (
            <Grid item key={product.id} xs={12} sm={6} md={4} lg={2}>
              <Card>
                <CardMedia
                  component="img"
                  height="140"
                  image={product.image}
                  alt={product.name}
                />
                <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                  <Typography variant="body1" bold sx={{ fontSize: '12px', fontWeight: 'bold' }}>{product.name}</Typography>
                  <Typography variant="body2" sx={{ display: 'flex', alignItems: 'center', fontSize: '12px' }}>
                    <StarIcon sx={{ color: '#FFD700', marginRight: '4px' }} />
                    {product.rating.toFixed(1)}
                  </Typography>
                  <Typography variant="body2" sx={{ display: 'flex', fontSize: '12px', marginTop: '8px' }}>
                    <span style={{ fontSize: '16px', color: 'black', marginRight: '4px',  fontWeight: 'bold'}}>
                      ${product.discountPrice.toFixed(2)}
                    </span>

                    <span style={{ fontSize: '10px', textDecoration: 'line-through', color: 'grey' }}>
                      ${product.price.toFixed(2)}
                    </span>
                    
                  </Typography>
                  <Button onClick={() => handleOrderClick(product.id)} variant="contained" fullWidth sx={{ backgroundColor: '#40BFFF', maxHeight: '25px', fontSize: '12px', marginTop: '8px' }}>
                    Add to cart
                  </Button>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Box>
  );
};

export default LandingContent;
